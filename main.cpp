/*
Key-Pair Generator (KPG)

Generates 2048-bit RSA key pair and stores key material in binary files:
	rsa-pri-key.bin
	rsa-pub-key.bin

Copyright (C) 2016  Daniel Randall

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see https://www.gnu.org/licenses/

	 build using the following command line:
		g++ -g3 -O2 -Wall -Wextra -o kpg main.cpp -lcryptopp -std=c++11 -lpthread -static
*/

#include <stdio.h>
#include <unistd.h>  
#include <pthread.h>

#include <iostream>
using namespace std;

#include "cryptopp/rsa.h"
#include "cryptopp/files.h"
#include "cryptopp/osrng.h"
using namespace CryptoPP;
using CryptoPP::RSA;

#define GMOD(_k) _k.GetModulus()
#define GPUX(_k) _k.GetPublicExponent()
#define GPRX(_k) _k.GetPrivateExponent()

#define PRFN "rsa-pri-key.bin"
#define PUFN "rsa-pub-key.bin"

#define LODK(_n, _k) {FileSource f(_n, true); _k.BERDecode(f);}
#define SAVK(_n, _k) {FileSink f(_n, true); _k.DEREncode(f);}

#define KEYSIZE 2048

// see: https://www.cryptopp.com/wiki/Keys_and_Formats

bool verifyRoundTrip(
	RSA::PrivateKey priL,
	RSA::PublicKey pubL)
{
	RSA::PrivateKey priR;
	RSA::PublicKey pubR;
	
	LODK(PRFN, priR);
	
	LODK(PUFN, pubR);
	
	bool priMod = (GMOD(priL) == GMOD(priR));
	bool priPriExp = (GPRX(priL) == GPRX(priR));
	bool priPubExp = (GPUX(priL) == GPUX(priR));
	
	bool priRoundTrip = (priMod && priPriExp && priPubExp);
		
	bool pubMod = (GMOD(pubL) == GMOD(pubR));
	bool pubPubExp = (GPUX(pubL) == GPUX(pubR));
	
	bool pubRoundTrip = (pubMod && pubPubExp);
	 
	return (priRoundTrip && pubRoundTrip);
}	

int main(int, char**)
{
	int result = 0;
	AutoSeededRandomPool asrp;
	InvertibleRSAFunction rsaPairgen;
   
	// generate private key:
	rsaPairgen.GenerateRandomWithKeySize(asrp, KEYSIZE);
		
	// validate private key:
	if (!rsaPairgen.Validate(asrp, 3))
	{
		result = -1;
		
		perror("Key not validated");
		
		return result;
	}	
	
	// extract private key:
	RSA::PrivateKey rsaPrivate(rsaPairgen);
	
	// extract public key:
	RSA::PublicKey rsaPublic(rsaPairgen);

	// save private key to file:
	SAVK(PRFN, rsaPrivate);
	
	// save public key to file:
	SAVK(PUFN, rsaPublic);
	
	bool verified =
		verifyRoundTrip(
			rsaPrivate,
			rsaPublic);
	
	if (verified)
	{
		puts("Successful");
		
		result = 0;
	}
	else
	{
		perror("Round-trip not verified");
		
		result = -2;
	}

	return result;
}